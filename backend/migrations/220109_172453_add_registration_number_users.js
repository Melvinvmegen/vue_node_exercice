module.exports = {
  up: async (q) => {
    await q.sequelize.query(
      "ALTER TABLE `users` ADD `registration_number` VARCHAR(255);"
    );
  },
  down: async () => {},
};